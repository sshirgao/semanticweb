__author__ = 'hacker'

import requests,json
import json as stockJSON
from datetime import datetime
import uuid
from tweepy.streaming import StreamListener
from tweepy import OAuthHandler
from tweepy import Stream
from ratelimiter import RateLimiter

# url = 'http://netflixroulette.net/api/api.php'
# url = 'http://reddit.com/user/shisoham/comments'
# callApi(url,{},{})
# # params={'q':'Now running Movies'}
# # soup = BeautifulSoup(callApi(url,{},{}), 'html.parser')
# # print soup._all_strings()

@RateLimiter(max_calls=3, period=1)
def getDirectorFromTheMovieDB(movieName,director):
    url = 'https://api.themoviedb.org/3/search/person?api_key=539b89083e087cefa06f8141a9be5ad8&language=en-US&query=Christopher%20Nolan&page=1'
    params = {'api_key':'539b89083e087cefa06f8141a9be5ad8','language':'en-US','query':director,'page':'1'}
    movies = callApi(url, params)
    movies = stockJSON.loads(movies)
    isKnownFor = []
    try:
        for i in movies['results'][0]['known_for']:
            isKnownFor.append(i['title'])
        return {'Director': director, 'KnownFor':isKnownFor, 'isDirectorOf':movieName}
    except:
        return {'Director': director, 'KnownFor':[], 'isDirectorOf':movieName}




def getDirectorsFromNetflixRoulette(movieName, director, file = None):
    url = 'http://netflixroulette.net/api/api.php'
    params = {}
    params['director'] = director
    # params['type'] = 'xml'
    print url, params
    movies = callApi(url, params)
    print movies
    if movies is not None:
        movies = stockJSON.loads(movies)
        isKnownFor = []
        try:
            for movie in movies:
                isKnownFor.append(movie['show_title'])
            return {'Director': director, 'KnownFor':isKnownFor, 'isDirectorOf':movieName}
        except Exception:
            return {'Director': director, 'KnownFor':[], 'isDirectorOf':movieName}
    else:
        return {'Director': director, 'KnownFor': [], 'isDirectorOf': movieName}
        # f = open(file,'a')
        # f.write(movies)
        # #add a new line
        # f.write("\n")
        # f.close()

@RateLimiter(max_calls=4, period=1)
def getReviewsFromNyTimes(movieName, file=None):
    apiKey = 'ab9160df5cc04121a869d0069b887aa9'
    url = 'https://api.nytimes.com/svc/movies/v2/reviews/search.json'
    params = {'api-key': apiKey,'query': movieName}
    reviews = callApi(url, params)
    reviews = stockJSON.loads(reviews)
    if len(reviews['results']) > 0:
        review = reviews['results'][0]
        return {'Critique':review['link']['url'], 'IsCritiqueOf':movieName}
    return {'Critique':'No review found', 'IsCritiqueOf':movieName}
    #
    # f = open(file,'a')
    # f.write(reviews)
    # f.close()
    # print "All Done"



def callApi(url, params, headers=None):
    if bool(headers):
        r = requests.get(url, params=params, headers = headers)
    else:
        r = requests.get(url, params=params)
    if r.status_code == 200:
        return r.content


def readFromJSONFile(fileName):
    file = open(fileName)
    fileContent = []
    jsonContent= stockJSON.loads(file.read())
    print jsonContent
    for line in jsonContent['omdbmovies']:
        print line
        movieInfo = {}
        movieInfo['FallsUnder'] = []
        for genre in line['genre'].split(','):
            movieInfo['FallsUnder'].append(genre)
        movieInfo['HasRating'] = line['imdb_rating']
        movieInfo['HasTitle'] = line['title']
        movieInfo['InYear'] = line['year']
        movieInfo['Director'] = line['director']
        movieInfo['HasActor'] = []
        for actor in line['actors'].split(','):
            movieInfo['HasActor'].append(actor)
        fileContent.append(movieInfo)
    return fileContent


#This is a basic listener that just prints received tweets to stdout.
class StdOutListener(StreamListener):

    def __init__(self, api=None):
        super(StdOutListener, self).__init__()
        self.num_tweets = 0

    def on_status(self, status):
        self.num_tweets += 1
        f = open('tweets.json', 'a')

        hasName = ""
        tweet = ""
        city = ""
        if (status.author.name != None):
            hasName = status.author.name.encode('utf8')
        if (status.text != None):
            tweet = status.text.encode('utf8')
        if (status.author.location != None):
            city = status.author.location.encode('utf8')

        f.write(json.dumps({"HasName":hasName,
                            "HasRetweetCount":status.retweet_count,
                            "HasFollowers": status.author.followers_count,
                            "Tweet":tweet,
                            "BelongsToCity": city}))
        f.write("\n")
        f.close()
        if self.num_tweets < 1000:
            return True
        else:
            return False

    def on_error(self, status):
        print status

# print  getDirectorFromTheMovieDB('Inception','Christopher Nolan')

def createDirectorsJson():
    omdbData = readFromJSONFile('omdbOscar.json')
    file = open('GoodData/listedDirectors.json','a')
    for oscarMovie in omdbData:
        file.write(stockJSON.dumps(getDirectorFromTheMovieDB(oscarMovie['HasTitle'],oscarMovie['Director'])))
        file.write('\n')
        print oscarMovie['HasTitle']
    file.close()

createDirectorsJson()



# if __name__ == "__main__":
#     omdbData = readFromJSONFile('omdbOscar.json')
#
#     # Variables that contains the user credentials to access Twitter API
#     # access_token = "4116969794-tLPUT7YG4mphyF3rqSC0xCwOXzdmNh54Io3S61X"
#     # access_token_secret = "GNUhLaEHP3koxCZmIjomL3DNwUoViWEUzmwZiBUgdPzgN"
#     # consumer_key = "SyMNs7ARJ6e2ZYjbQxQIsdpHv"
#     # consumer_secret = "e3eG9oBbOPUxErq00KcXMGeWTpsFYVNu0XpxRwlAQAe6lxhWOF"
#
#     movieTitles = []
#     linkedJSON = {}
#
#     for oscarMovie in omdbData:
#         print oscarMovie['HasTitle']
#         movieTitles.append("#"+(oscarMovie['HasTitle']).replace(" ", ""))
#         # directorDict['Director'] = getDirectorFromTheMovieDB(oscarMovie['HasTitle'],oscarMovie['Director'])
#         # oscarMovie['Critique'] = getReviewsFromNyTimes(oscarMovie['HasTitle'])
#         linkedJSON[oscarMovie['HasTitle']]=getReviewsFromNyTimes(oscarMovie['HasTitle'])
#         print oscarMovie['HasTitle']
#
#
#     # if (movieTitles):
#     #     # This handles Twitter authetification and the connection to Twitter Streaming API
#     #     l = StdOutListener()
#     #     auth = OAuthHandler(consumer_key, consumer_secret)
#     #     auth.set_access_token(access_token, access_token_secret)
#     #     stream = Stream(auth, l)
#     #
#     #     # This line filter Twitter Streams to capture data by the movieTitles
#     #     stream.filter(track=movieTitles)
#
#     file = open('linkedReviewData.json','a')
#     file.write(stockJSON.dumps(linkedJSON))
#     file.close()
#     # for i in linkedJSON:
