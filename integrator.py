__author__ = 'hacker'

import json

def readFromJSONFile(fileName):
    file = open(fileName)
    fileContent = []
    for line in file:
        movieInfo = {}
        line = json.loads(line)
        movieInfo['FallsUnder'] = []
        for genre in line['genre'].split(','):
            movieInfo['FallsUnder'].append(genre)
        movieInfo['HasRating'] = line['imdb_rating']
        movieInfo['HasTitle'] = line['title']
        movieInfo['InYear'] = line['year']
        movieInfo['Director'] = line['director']
        movieInfo['HasActor'] = []
        for actor in line['actors'].split(','):
            movieInfo['HasActor'].append(actor)
        fileContent.append(movieInfo)
    return fileContent

def getDataFromFile(fileName):
    file = open(fileName)
    jsonData = json.loads(file.read())
    file.close()
    return jsonData



tweetsFile = getDataFromFile('tweets.json')
nyTimesFile = getDataFromFile('linkedReviewData.json')
TmdbFile = getDataFromFile('linkedData.json')
omdbData = readFromJSONFile('omdbOscar.json')

finalData = {'movies':[]}

i = 0
while i < 4:
    oscarMovie = omdbData[i]
    oscarMovie['Director'] = TmdbFile[oscarMovie['HasTitle']]
    oscarMovie['Critique'] = nyTimesFile[oscarMovie['HasTitle']]
    oscarMovie['Everyman'] = tweetsFile[oscarMovie['HasTitle']]
    finalData['movies'].append(oscarMovie)
    print oscarMovie['HasTitle']
    i += 1

file = open('finalLinkedData.json','w')
file.write(json.dumps(finalData))
file.close()






