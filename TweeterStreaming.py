from traitlets.config.application import catch_config_error

__author__ = 'hacker'

import requests,json
import json as stockJSON
from datetime import datetime
import uuid
from tweepy.streaming import StreamListener
from tweepy import OAuthHandler
from tweepy import Stream
import copy

# url = 'http://netflixroulette.net/api/api.php'
# url = 'http://reddit.com/user/shisoham/comments'
# callApi(url,{},{})
# # params={'q':'Now running Movies'}
# # soup = BeautifulSoup(callApi(url,{},{}), 'html.parser')
# # print soup._all_strings()

movieFilter=""
globalDic={}
f = open('tweets.json', 'a')
def getDirectorsFromNetflixRoulette(movieName, director, file = None):
    url = 'http://netflixroulette.net/api/api.php'
    params = {}
    params['director'] = director
    # params['type'] = 'xml'
    movies = callApi(url, params)
    if movies is not None:
        movies = stockJSON.loads(movies)
        isKnownFor = []
        try:
            for movie in movies:
                isKnownFor.append(movie['show_title'])
            return {'Director': director, 'KnownFor':isKnownFor, 'isDirectorOf':movieName}
        except Exception:
            return {'Director': director, 'KnownFor':[], 'isDirectorOf':movieName}
    else:
        return {'Director': director, 'KnownFor': [], 'isDirectorOf': movieName}
        # f = open(file,'a')
        # f.write(movies)
        # #add a new line
        # f.write("\n")
        # f.close()


def getReviewsFromNyTimes(movieName, file=None):
    apiKey = 'ab9160df5cc04121a869d0069b887aa9'
    url = 'https://api.nytimes.com/svc/movies/v2/reviews/search.json'
    params = {'api-key': apiKey,'query': movieName}
    reviews = callApi(url, params)
    reviews = stockJSON.loads(reviews)
    if len(reviews['results']) > 0:
        review = reviews['results'][0]
        return {'Critique':review['link']['url'], 'IsCritiqueOf':movieName}
    return {'Critique':'No review found', 'IsCritiqueOf':movieName}
    #
    # f = open(file,'a')
    # f.write(reviews)
    # f.close()
    # print "All Done"



def callApi(url, params, headers=None):
    if bool(headers):
        r = requests.get(url, params=params, headers = headers)
    else:
        r = requests.get(url, params=params)
    if r.status_code == 200:
        return r.content
    print r.content

def readFromJSONFile(fileName):
    file = open(fileName)
    fileContent = []
    jsonContent= stockJSON.loads(file.read())
    print jsonContent
    for line in jsonContent['omdbmovies']:
        print line
        movieInfo = {}
        movieInfo['FallsUnder'] = []
        for genre in line['genre'].split(','):
            movieInfo['FallsUnder'].append(genre)
        movieInfo['HasRating'] = line['imdb_rating']
        movieInfo['HasTitle'] = line['title']
        movieInfo['InYear'] = line['year']
        movieInfo['Director'] = line['director']
        movieInfo['HasActor'] = []
        for actor in line['actors'].split(','):
            movieInfo['HasActor'].append(actor)
        fileContent.append(movieInfo)
    return fileContent


#This is a basic listener that just prints received tweets to stdout.
class StdOutListener(StreamListener):

    def __init__(self, api=None):
        super(StdOutListener, self).__init__()
        self.num_tweets = 0

    def on_status(self, status):
        self.num_tweets += 1
        hasName = ""
        tweet = ""
        city = ""
        tweeter = []
        if (status.author.name != None):
            hasName = status.author.name.encode('utf8')
        if (status.text != None):
            tweet = status.text.encode('utf8')
        if (status.author.location != None):
            city = status.author.location.encode('utf8')

        dic={"HasName":hasName,
                            "HasRetweetCount":status.retweet_count,
                            "HasFollowers": status.author.followers_count,
                            "Tweet":tweet,
                            "BelongsToCity": city}
        print tweeter
        if self.num_tweets < 13000:

            f.write(json.dumps(dic))
            f.write("\n")

            return True
        else:
            return False

    def on_error(self, status):
        print status


if __name__ == "__main__":
    omdbData = readFromJSONFile('omdbOscar.json')

    # Variables that contains the user credentials to access Twitter API
    access_token = "4116969794-tLPUT7YG4mphyF3rqSC0xCwOXzdmNh54Io3S61X"
    access_token_secret = "GNUhLaEHP3koxCZmIjomL3DNwUoViWEUzmwZiBUgdPzgN"
    consumer_key = "SyMNs7ARJ6e2ZYjbQxQIsdpHv"
    consumer_secret = "e3eG9oBbOPUxErq00KcXMGeWTpsFYVNu0XpxRwlAQAe6lxhWOF"
    try:
        #for oscarMovie in omdbData:
        #movieTitle="#"+(oscarMovie['HasTitle']).replace(" ", "")
        l = StdOutListener()
        auth = OAuthHandler(consumer_key, consumer_secret)
        auth.set_access_token(access_token, access_token_secret)
        stream = Stream(auth, l)
        # This line filter Twitter Streams to capture data by the movieTitles
        stream.filter(track="Batman")
    except Exception:
        f.close()


