package mypkg;

import java.io.IOException;
import java.io.InputStream;
import java.util.ArrayList;
import java.util.List;

import com.hp.hpl.jena.query.Query;
import com.hp.hpl.jena.query.QueryExecution;
import com.hp.hpl.jena.query.QueryExecutionFactory;
import com.hp.hpl.jena.query.QueryFactory;
import com.hp.hpl.jena.query.QuerySolution;
import com.hp.hpl.jena.query.ResultSet;
import com.hp.hpl.jena.rdf.model.Model;
import com.hp.hpl.jena.rdf.model.ModelFactory;
import com.hp.hpl.jena.rdf.model.RDFNode;
import com.hp.hpl.jena.util.FileManager;

public class MovieDataSet {

	String omdb_title;
	String omdb_year;
	String omdb_actors;
	String omdb_director;
	String omdb_genre;
	String omdb_awards;
	String omdb_rating;
	String omdb_runtime;
	String omdb_releaseDate;
	String omdb_country;
	String omdb_language;
	String omdb_poster;
	
	String nyTimes_IsCritiqueOf;
	String nyTimes_name;
	String nyTimes_Conveys;
	
	String dir_IsDirectorOf;
	String dir_name;
	String dir_KnownFor;
	
	String tweet_HasMovieTag;
	List<String> tweet_name;
	List<String> tweet_Tweets;
	List<String> tweet_HasRetweetCount;
	List<String> tweet_HasFollowers;
	List<String> tweet_BelongsToCity;
	
	public String getOmdb_title() {
		return omdb_title;
	}

	public void setOmdb_title(String omdb_title) {
		this.omdb_title = omdb_title;
	}

	public String getOmdb_year() {
		return omdb_year;
	}

	public void setOmdb_year(String omdb_year) {
		this.omdb_year = omdb_year;
	}

	public String getOmdb_actors() {
		return omdb_actors;
	}

	public void setOmdb_actors(String omdb_actors) {
		this.omdb_actors = omdb_actors;
	}

	public String getOmdb_director() {
		return omdb_director;
	}

	public void setOmdb_director(String omdb_director) {
		this.omdb_director = omdb_director;
	}

	public String getOmdb_genre() {
		return omdb_genre;
	}

	public void setOmdb_genre(String omdb_genre) {
		this.omdb_genre = omdb_genre;
	}

	public String getOmdb_awards() {
		return omdb_awards;
	}

	public void setOmdb_awards(String omdb_awards) {
		this.omdb_awards = omdb_awards;
	}

	public String getOmdb_rating() {
		return omdb_rating;
	}

	public void setOmdb_rating(String omdb_rating) {
		this.omdb_rating = omdb_rating;
	}

	public String getOmdb_runtime() {
		return omdb_runtime;
	}

	public void setOmdb_runtime(String omdb_runtime) {
		this.omdb_runtime = omdb_runtime;
	}

	public String getOmdb_releaseDate() {
		return omdb_releaseDate;
	}

	public void setOmdb_releaseDate(String omdb_releaseDate) {
		this.omdb_releaseDate = omdb_releaseDate;
	}

	public String getOmdb_country() {
		return omdb_country;
	}

	public void setOmdb_country(String omdb_country) {
		this.omdb_country = omdb_country;
	}

	public String getOmdb_language() {
		return omdb_language;
	}

	public void setOmdb_language(String omdb_language) {
		this.omdb_language = omdb_language;
	}

	public String getOmdb_poster() {
		return omdb_poster;
	}

	public void setOmdb_poster(String omdb_poster) {
		this.omdb_poster = omdb_poster;
	}

	public String getNyTimes_IsCritiqueOf() {
		return nyTimes_IsCritiqueOf;
	}

	public void setNyTimes_IsCritiqueOf(String nyTimes_IsCritiqueOf) {
		this.nyTimes_IsCritiqueOf = nyTimes_IsCritiqueOf;
	}

	public String getNyTimes_name() {
		return nyTimes_name;
	}

	public void setNyTimes_name(String nyTimes_name) {
		this.nyTimes_name = nyTimes_name;
	}

	public String getNyTimes_Conveys() {
		return nyTimes_Conveys;
	}

	public void setNyTimes_Conveys(String nyTimes_Conveys) {
		this.nyTimes_Conveys = nyTimes_Conveys;
	}

	public String getDir_IsDirectorOf() {
		return dir_IsDirectorOf;
	}

	public void setDir_IsDirectorOf(String dir_IsDirectorOf) {
		this.dir_IsDirectorOf = dir_IsDirectorOf;
	}

	public String getDir_name() {
		return dir_name;
	}

	public void setDir_name(String dir_name) {
		this.dir_name = dir_name;
	}

	public String getDir_KnownFor() {
		return dir_KnownFor;
	}

	public void setDir_KnownFor(String dir_KnownFor) {
		this.dir_KnownFor = dir_KnownFor;
	}

	public String getTweet_HasMovieTag() {
		return tweet_HasMovieTag;
	}

	public void setTweet_HasMovieTag(String tweet_HasMovieTag) {
		this.tweet_HasMovieTag = tweet_HasMovieTag;
	}

	public List<String> getTweet_name() {
		return tweet_name;
	}

	public void setTweet_name(String tweet_name) {
		(this.tweet_name).add(tweet_name) ;
	}

	public List<String> getTweet_Tweets() {
		return tweet_Tweets;
	}

	public void setTweet_Tweets(String tweet_Tweets) {
		this.tweet_Tweets.add( tweet_Tweets);
	}

	public List<String> getTweet_HasRetweetCount() {
		return tweet_HasRetweetCount;
	}

	public void setTweet_HasRetweetCount(String tweet_HasRetweetCount) {
		(this.tweet_HasRetweetCount).add( tweet_HasRetweetCount);
	}

	public List<String> getTweet_HasFollowers() {
		return tweet_HasFollowers;
	}

	public void setTweet_HasFollowers(String tweet_HasFollowers) {
		(this.tweet_HasFollowers).add(tweet_HasFollowers);
	}

	public List<String> getTweet_BelongsToCity() {
		return tweet_BelongsToCity;
	}

	public void setTweet_BelongsToCity(String tweet_BelongsToCity) {
		(this.tweet_BelongsToCity).add(tweet_BelongsToCity);
	}

	public void tweet_query(String str)
	{
		tweet_runQuery("select ?HasMovieTag ?name ?Tweets ?HasRetweetCount ?HasFollowers ?BelongsToCity where {  ?tweetr topMovies:HasMovieTag '"+ str +"'. ?tweetr topMovies:HasMovieTag ?HasMovieTag.   ?tweetr foaf:name ?name.   ?tweetr topMovies:Tweets ?Tweets.  ?tweetr topMovies:HasRetweetCount ?HasRetweetCount.  ?tweetr topMovies:HasFollowers ?HasFollowers.  ?tweetr topMovies:BelongsToCity ?BelongsToCity.}", this._twitter);
	}
	
	public void omdb_Query(String str)
	{
		omdb_runQuery("select ?HasTitle ?InYear ?HasActor ?HasDirector "
				+ "?FallsUnderGenre ?HasWonAwards ?HasRating ?HasRuntime"
				+ "?ReleaseDate ?Country ?Language ?HasPoster ?Test"
				+ "where { "
				+ "?movie topMovies:HasTitle '"+ str + "'."
				+ "?movie topMovies:HasTitle ?HasTitle."
				+ "?movie topMovies:InYear ?InYear."
				+ "?movie topMovies:HasActor ?HasActor."
				+ "?movie topMovies:HasDirector ?HasDirector."
				+ "?movie topMovies:FallsUnderGenre ?FallsUnderGenre."
				+ "?movie topMovies:HasWonAwards ?HasWonAwards."
				+ "?movie topMovies:HasRating ?HasRating."
				+ "?movie topMovies:HasRuntime ?HasRuntime."
				+ "?movie topMovies:ReleaseDate ?ReleaseDate."
				+ "?movie topMovies:BelongsToCountry ?Country."
				+ "?movie topMovies:Language ?Language."
				+ "?movie topMovies:HasPoster ?HasPoster."
				+ "?movie topMovies:Test ?Test."
				+ "}", this._movie);
	}

	public void nyTimes_Query(String str)
	{
		nyTimes_runQuery("select ?IsCritiqueOf ?name ?Conveys ?TTest"
				+ "where { "
				+ "?rev topMovies:IsCritiqueOf '"+ str + "'."
				+ "?rev topMovies:IsCritiqueOf ?IsCritiqueOf."
				+ "?rev foaf:name ?name."
				+ "?rev topMovies:Conveys ?Conveys."
				+ "?rev topMovies:TTest ?TTest."
				+ "}", this._review);
	}
	
	public void directors_Query(String str)
	{
		directors_runQuery("select ?IsDirectorOf ?name ?KnownFor ?DTest"
				+ "where { "
				+ "?dirList topMovies:IsDirectorOf '"+ str + "'."
				+ "?dirList topMovies:IsDirectorOf ?IsDirectorOf."
				+ "?dirList foaf:name ?name."
				+ "?dirList topMovies:KnownFor ?KnownFor."
				+ "?dirList topMovies:DTest ?DTest."
				+ "}", this._director);
	}
	

	private void tweet_runQuery(String queryRequest, Model model)
	{
		StringBuffer queryStr = new StringBuffer();

		// Establish Prefixes
		queryStr.append("PREFIX topMovies: <" + defaultNameSpace + "> ");
		queryStr.append("PREFIX rdfs: <http://www.w3.org/2000/01/rdf-schema#> ");
		queryStr.append("PREFIX rdf: <http://www.w3.org/1999/02/22-rdf-syntax-ns#> ");
		queryStr.append("PREFIX foaf: <http://xmlns.com/foaf/0.1/> ");
		queryStr.append("PREFIX owl: <http://www.w3.org/2002/07/owl#> ");
		queryStr.append("PREFIX xsd: <http://www.w3.org/2001/XMLSchema#> ");

		//Now add query
		queryStr.append(queryRequest);
		Query query = QueryFactory.create(queryStr.toString());
		QueryExecution qexec = QueryExecutionFactory.create(query, model);

		try 
		{
			ResultSet response = qexec.execSelect();
			while( response.hasNext())
			{
				QuerySolution soln = response.nextSolution();
				RDFNode HasMovieTag = soln.get("?HasMovieTag");
				RDFNode name = soln.get("?name");
				RDFNode Tweets= soln.get("?Tweets");
				RDFNode HasRetweetCount= soln.get("?HasRetweetCount");
				RDFNode HasFollowers= soln.get("?HasFollowers");
				RDFNode BelongsToCity= soln.get("?BelongsToCity");
				if( HasMovieTag != null ){
					System.out.println( "HasMovieTag " + HasMovieTag.toString() );
					setTweet_HasMovieTag(HasMovieTag.toString());
				}
				if( name != null ){
					System.out.println( "name " + name.toString() );
					setTweet_name(name.toString());
				}
				if( Tweets != null ){
					System.out.println( "Tweets " + Tweets.toString() );
					setTweet_Tweets(Tweets.toString());
				}
				if( HasRetweetCount != null ){
					System.out.println( "HasRetweetCount " + HasRetweetCount.toString() );
					setTweet_HasRetweetCount(HasRetweetCount.toString());
				}
				if( HasFollowers != null ){
					System.out.println( "HasFollowers " + HasFollowers.toString() );
					setTweet_HasFollowers(HasFollowers.toString() );
				}
				if( BelongsToCity != null ){
					System.out.println( "BelongsToCity " + BelongsToCity.toString() );
					setTweet_BelongsToCity(BelongsToCity.toString());
				}

			}
		}
		finally { qexec.close();}
	}
	
	private void omdb_runQuery(String queryRequest, Model model)
	{
		StringBuffer queryStr = new StringBuffer();

		// Establish Prefixes
		queryStr.append("PREFIX topMovies: <" + defaultNameSpace + "> ");
		queryStr.append("PREFIX rdfs: <http://www.w3.org/2000/01/rdf-schema#> ");
		queryStr.append("PREFIX rdf: <http://www.w3.org/1999/02/22-rdf-syntax-ns#> ");
		queryStr.append("PREFIX foaf: <http://xmlns.com/foaf/0.1/> ");
		queryStr.append("PREFIX owl: <http://www.w3.org/2002/07/owl#> ");
		queryStr.append("PREFIX xsd: <http://www.w3.org/2001/XMLSchema#> ");

		//Now add query
		queryStr.append(queryRequest);
		Query query = QueryFactory.create(queryStr.toString());
		QueryExecution qexec = QueryExecutionFactory.create(query, model);

		try 
		{
			ResultSet response = qexec.execSelect();
			while( response.hasNext())
			{
				QuerySolution soln = response.nextSolution();
				RDFNode name = soln.get("?HasTitle");
				RDFNode year = soln.get("?InYear");
				RDFNode actors = soln.get("?HasActor");
				RDFNode director = soln.get("?HasDirector");
				RDFNode genre = soln.get("?FallsUnderGenre");
				RDFNode awards = soln.get("?HasWonAwards");
				RDFNode rating = soln.get("?HasRating");
				RDFNode runtime = soln.get("?HasRuntime");
				RDFNode releaseDate = soln.get("?ReleaseDate");
				RDFNode country = soln.get("?Country");
				RDFNode language = soln.get("?Language");
				RDFNode poster = soln.get("?HasPoster");
				
				if( name != null ){
					System.out.println( "Movie Title: " + name.toString() );
					setOmdb_title(name.toString());
				}
				if( year != null ){
					System.out.println( "Year: " + year.toString() );
					setOmdb_year(year.toString());
				}
				if( actors != null ){
					System.out.println( "Actors: " + actors.toString() );
					setOmdb_actors(actors.toString());
				}
				if( director != null ){
					System.out.println( "Director: " + director.toString() );
					setOmdb_director(director.toString());
				}
				if( genre != null ){
					System.out.println( "Genre: " + genre.toString() );
					setOmdb_genre(genre.toString());
				}
				if( awards != null ){
					System.out.println( "Awards: " + awards.toString() );
					setOmdb_awards(awards.toString());
				}
				if( rating != null ){
					System.out.println( "Rating: " + rating.toString() );
					setOmdb_rating(rating.toString());
				}
				if( runtime != null ){
					System.out.println( "Runtime: " + runtime.toString() );
					setOmdb_runtime(runtime.toString());
				}
				if( releaseDate != null ){
					System.out.println( "Release Date: " + releaseDate.toString() );
					setOmdb_releaseDate(releaseDate.toString());
				}
				if( country != null ){
					System.out.println( "Country: " + country.toString() );
					setOmdb_country(country.toString());
				}
				if( language != null ){
					System.out.println( "Language: " + language.toString());
					setOmdb_language(language.toString());
				}
				if( poster != null ){
					System.out.println( "Poster: " + poster.toString());
					setOmdb_poster(poster.toString());
				}
			}
		}
		finally { qexec.close();}
	}
	
	private void nyTimes_runQuery(String queryRequest, Model model)
	{
		StringBuffer queryStr = new StringBuffer();

		// Establish Prefixes
		queryStr.append("PREFIX topMovies: <" + defaultNameSpace + "> ");
		queryStr.append("PREFIX rdfs: <http://www.w3.org/2000/01/rdf-schema#> ");
		queryStr.append("PREFIX rdf: <http://www.w3.org/1999/02/22-rdf-syntax-ns#> ");
		queryStr.append("PREFIX foaf: <http://xmlns.com/foaf/0.1/> ");
		queryStr.append("PREFIX owl: <http://www.w3.org/2002/07/owl#> ");
		queryStr.append("PREFIX xsd: <http://www.w3.org/2001/XMLSchema#> ");

		//Now add query
		queryStr.append(queryRequest);
		Query query = QueryFactory.create(queryStr.toString());
		QueryExecution qexec = QueryExecutionFactory.create(query, model);

		try 
		{
			ResultSet response = qexec.execSelect();
			while( response.hasNext())
			{
				QuerySolution soln = response.nextSolution();
				RDFNode title = soln.get("?IsCritiqueOf");
				RDFNode name = soln.get("?name");
				RDFNode review = soln.get("?Conveys");
				
				if( title != null ){
					System.out.println( "Movie Title: " + title.toString());
					setNyTimes_name(title.toString());
				}
				if( name != null ){
					System.out.println( "Reviewer: " + name.toString());
					setNyTimes_name(name.toString());
				}
				if( review != null ){
					System.out.println( "Review URI: " + review.toString());
					setNyTimes_name(review.toString());
				}
			}
		}
		finally { qexec.close();}
	}

	private void directors_runQuery(String queryRequest, Model model)
	{
		StringBuffer queryStr = new StringBuffer();

		// Establish Prefixes
		queryStr.append("PREFIX topMovies: <" + defaultNameSpace + "> ");
		queryStr.append("PREFIX rdfs: <http://www.w3.org/2000/01/rdf-schema#> ");
		queryStr.append("PREFIX rdf: <http://www.w3.org/1999/02/22-rdf-syntax-ns#> ");
		queryStr.append("PREFIX foaf: <http://xmlns.com/foaf/0.1/> ");
		queryStr.append("PREFIX owl: <http://www.w3.org/2002/07/owl#> ");
		queryStr.append("PREFIX xsd: <http://www.w3.org/2001/XMLSchema#> ");

		//Now add query
		queryStr.append(queryRequest);
		Query query = QueryFactory.create(queryStr.toString());
		QueryExecution qexec = QueryExecutionFactory.create(query, model);

		try 
		{
			ResultSet response = qexec.execSelect();
			while( response.hasNext())
			{
				QuerySolution soln = response.nextSolution();
				RDFNode title = soln.get("?IsDirectorOf");
				RDFNode name = soln.get("?name");
				RDFNode otherTitles = soln.get("?KnownFor");
				
				if( title != null ){
					System.out.println( "Movie Title: " + title.toString());
					setNyTimes_name(title.toString());
				}
				if( name != null ){
					System.out.println( "Director: " + name.toString());
					setNyTimes_name(name.toString());
				}
				if( otherTitles != null ){
					System.out.println( "Also known for: " + otherTitles.toString());
					setNyTimes_name(otherTitles.toString());
				}
			}
		}
		finally { qexec.close();}
	}
	

	static String defaultNameSpace = "http://www.semanticweb.org/manohara/ontologies/2016/9/untitled-ontology-10#";
	Model _movie = null;
	Model _review = null;
	Model _director = null;
	Model _twitter = null;


	MovieDataSet() throws IOException
	{
		tweet_name=new ArrayList<String>();
		tweet_Tweets=new ArrayList<String>();
		tweet_HasRetweetCount=new ArrayList<String>();
		tweet_HasFollowers=new ArrayList<String>();
		tweet_BelongsToCity=new ArrayList<String>();

		_movie = ModelFactory.createOntologyModel();
		InputStream inOmdbInstance = FileManager.get().open("Ontologies/omdb.rdf");
		_movie.read(inOmdbInstance,defaultNameSpace);
		inOmdbInstance.close();

		_review = ModelFactory.createOntologyModel();
		InputStream inRevInstance = FileManager.get().open("Ontologies/nyTimes.rdf");
		_review.read(inRevInstance,defaultNameSpace);
		inRevInstance.close();

		_director = ModelFactory.createOntologyModel();
		InputStream inDirInstance = FileManager.get().open("Ontologies/directors.rdf");
		_director.read(inDirInstance,defaultNameSpace);
		inDirInstance.close();

		_twitter=ModelFactory.createOntologyModel();
		InputStream inTwtInstance = FileManager.get().open("Ontologies/twitter.rdf");
		_twitter.read(inTwtInstance,defaultNameSpace);
		inTwtInstance.close();
	}
}
