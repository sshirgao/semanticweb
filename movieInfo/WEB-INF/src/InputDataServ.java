package mypkg;

import javax.servlet.*;
import javax.servlet.http.*;
import org.w3c.dom.Document;
import org.w3c.dom.Element;

import java.util.*;
import java.io.*;
import java.net.*;
import java.io.File;

public class InputDataServ extends javax.servlet.http.HttpServlet {

	private String search="";
	private boolean failure = false;

	public void doGet(HttpServletRequest req, HttpServletResponse res) throws ServletException, IOException {
		// Do nothing
	}

	public void doPost(HttpServletRequest req, HttpServletResponse res) throws ServletException, IOException {

		res.setContentType("text/html");
		PrintWriter out = res.getWriter();

		String srch=req.getParameter("search");   
		
		if (srch != null && srch != "")
		{	
			MovieDataSet data = new MovieDataSet();
			String str1 = srch; //"The Patriot";
			//String str2 = "#"+str1.replace(" ", "");//"#ThePatriot";
			data.omdb_Query(str1);
			data.nyTimes_Query(str1);
			data.directors_Query(str1);
			data.tweet_query("#"+str1.replace(" ", ""));
			
						
			
			if (failure) {
				out.println("<html><body><h1>Failure!!</h2></br></body></html>");
			} else {
				out.println("<html><body>");
				
				out.println("<h1>Success!</h1></br><p>Movie Title: " + data.getOmdb_title()
						+ " </p>");
				out.println("<h1>Success!</h1></br><p>Search result: " + data.getTweet_name()
						+ " </p>");
				
				out.println("</body></html>");
			}
			
		// Hyperlink back to the web form page
		out.println("<p><a href='webform.html'>Back to Movie Info search!!! </a></p>");
		} else
		{
			RequestDispatcher rd = req.getRequestDispatcher("/webform.html");
			rd.include(req, res);
		}	
	}

}
