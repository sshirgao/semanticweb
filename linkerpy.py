__author__ = 'hacker'
print "You either die a hero or you live long enough to see yourself become the villian"

# import xml.etree.ElementTree as et
from xml.parsers import expat
import io
from lxml import etree as et
import os

def getDirectorMapping():
    directors={}
    tree = et.parse('GoodData/commaSeparatedDirectors-json.rdf')
    root = tree.getroot()
    for child in root:
        directors[child[1].text.lower()] = child.attrib[child.attrib.keys()[0]]
    return directors

def getMovieMapping():
    movies = {}
    tree = et.parse('GoodData/omdbOscar-json.rdf')
    root = tree.getroot()
    for child in root:
        movies[child[2].text.lower()] = child.attrib[child.attrib.keys()[0]]
    return movies


def mapDirectorsInMovies():
    directors = getDirectorMapping()
    tree = et.parse('GoodData/omdbOscar-json.rdf')
    root = tree.getroot()
    for child in root:
        director = child[5].text.lower()
        child[5].text = ''
        child[5].set('{http://www.w3.org/1999/02/22-rdf-syntax-ns#}resource',directors[director])
    tree.write('linkedData/omdb.rdf')

def mapMoviesInDirectors():
    movies = getMovieMapping()
    tree = et.parse('GoodData/commaSeparatedDirectors-json.rdf')
    root = tree.getroot()
    for child in root:
        movie = child[2].text.lower()
        child[2].text = ''
        child[2].set('{http://www.w3.org/1999/02/22-rdf-syntax-ns#}resource',movies[movie])
    tree.write('linkedData/directors.rdf')

def mapMovieInCritique():
    movies = getMovieMapping()
    tree = et.parse('GoodData/nyTimes-json.rdf')
    root = tree.getroot()
    for child in root:
        movie = child[3].text.lower()
        child[3].text = ''
        try:
            child[3].set('{http://www.w3.org/1999/02/22-rdf-syntax-ns#}resource',movies[movie])
        except:
            child[3].text = movie
            print "Exception occurred for",movie
    tree.write('linkedData/nyTimes.rdf')

def mapMovieInTweetFile():
    movies = getMovieMapping()
    newMovies = {}
    for movie in movies.keys():
        newMovie = movie.replace(" ","")
        newMovie = "#"+newMovie
        newMovies[newMovie] = movies[movie]
    tree = et.parse('GoodData/cleanedTweetNewFormatForOld-json.rdf')
    root = tree.getroot()
    for child in root:
        try:
            movieTag = child[1].text.lower()
            subelem = et.Element('{http://www.semanticweb.org/manohara/ontologies/2016/9/untitled-ontology-10#}OfMovie')
            subelem.set('{http://www.w3.org/1999/02/22-rdf-syntax-ns#}resource',newMovies[movieTag])
            child.append(subelem)
        except Exception as e:
            print e
    tree.write('linkedData/twitter.rdf')


# def cleanTweetFile():
#     with io.open('d.txt','r',encoding='utf-8',errors='ignore') as infile, \
#      io.open('d_parsed.txt','w',encoding='ascii',errors='ignore') as outfile:
#         for line in infile:
#             print(*line.split(), file=outfile)

def cleanTweetFile():
    f = open('GoodData/tweetNewFormatForOld-json.rdf')
    f2 = open('GoodData/cleanedTweetNewFormatForOld-json.rdf','w')
    for line in f:
        line=line.decode('utf-8','ignore').encode("utf-8")
        f2.write(line)
    f.close()
    f2.close()



# cleanTweetFile()
# mapDirectorsInMovies()
# mapMoviesInDirectors()
# mapMovieInCritique()
mapMovieInTweetFile()
print "All done"