package org.semweb.proj142016;

import java.io.IOException;
import java.io.InputStream;

import org.apache.jena.riot.thrift.wire.RDF_IRI;

import com.hp.hpl.jena.rdf.model.*;
import com.hp.hpl.jena.util.FileManager;
import com.hp.hpl.jena.query.Query;
import com.hp.hpl.jena.query.QueryExecution;
import com.hp.hpl.jena.query.QueryExecutionFactory;
import com.hp.hpl.jena.query.QueryFactory;
import com.hp.hpl.jena.query.QuerySolution;
import com.hp.hpl.jena.query.ResultSet;

/**
 * @author manohara
 */
public class MovieInfo {

	static String defaultNameSpace = "http://www.semanticweb.org/manohara/ontologies/2016/9/untitled-ontology-10#";
	
	Model _movie = null;
	Model _review = null;
	Model _director = null;
	
	public static void main(String[] args) throws IOException {
		
		MovieDataSet data = new MovieDataSet();
		String str1 = "The Patriot";
		//String str2 = "#"+str1.replace(" ", "");//"#ThePatriot";
		data.omdb_Query(str1);
		data.nyTimes_Query(str1);
		data.directors_Query(str1);
		data.tweet_query("#"+str1.replace(" ", ""));
	}
}
